#!/bin/bash

mkdir -p /opt/rpy-temp

systemctl stop rpy-cpu-temp
systemctl disable rpy-cpu-temp.timer
systemctl disable rpy-cpu-temp.service

cd /opt/rpy-temp

cp -f * /opt/rpy-temp/

chmod +x /opt/rpy-temp/*.py

ln -f -s /opt/rpy-temp/rpy-cpu-temp.timer /etc/systemd/system/rpy-cpu-temp.timer
ln -f -s /opt/rpy-temp/rpy-cpu-temp.service /etc/systemd/system/rpy-cpu-temp.service
ln -f -s /opt/rpy-temp/logrotate.conf /etc/logrotate.d/rpy-cpu-temp

#logrotate -vf /etc/logrotate.conf

systemctl daemon-reload
systemctl enable rpy-cpu-temp.timer
systemctl start rpy-cpu-temp
systemctl status rpy-cpu-temp

