#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# rpy-cpu-temp.py

import os
import sys
import subprocess
from datetime import datetime

LOG_FILE = os.environ.get('LOG_FILE', '/var/log/rpy-cpu-temp.log')
UNIT = os.environ.get('UNIT', 'C')
HOT = int(os.environ.get('HOT', 70))
COLD = int(os.environ.get('COLD', 30))
WARNING = int(os.environ.get('WARNING', 60))

p = subprocess.Popen(['vcgencmd', 'measure_temp'],
                     stdout=subprocess.PIPE, stderr=subprocess.PIPE
                     )

temp, unit = p.communicate()[0].decode(sys.stdout.encoding). \
    split("=")[1].split("'")

message = "INFO"

temp = float(temp)

if temp < COLD:
    message = "COLD"
elif temp > HOT:
    message = "DANGER"
elif temp > WARNING:
    message = "WARNING"

with open(LOG_FILE, 'a') as log:
    log.write(
        f"{datetime.now().astimezone().isoformat()} {message} {temp} {unit}"
        )
