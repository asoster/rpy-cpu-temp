# RaspberryPy Temp Service

Simple service to log and monitoring CPU temperature at raspberrrypy.


## Get code

You must get this repo code.

## Install

as pi user, run with `sudo` command the install script:

```
$ sudo bash install.sh
```

## Get info log

```
$ tail -f /var/log/rpy-cpu-temp.log
2020-08-13T21:34:32,232899527-03:00 INFO 59.1 C
2020-08-13T21:34:52,252083724-03:00 WARNING 62.3 C
2020-08-13T21:35:09,291644284-03:00 WARNING 62.3 C
2020-08-13T21:35:28,315783715-03:00 WARNING 65.5 C
2020-08-13T21:35:47,260913932-03:00 WARNING 65.5 C
2020-08-13T21:36:05,168771113-03:00 WARNING 64.5 C
2020-08-13T21:36:21,244456188-03:00 WARNING 67.7 C

```

